trigger Account_Opportunity on Account (after insert, after update, before delete, after delete) {
 // Automatic creating Opportunity when account created
    if(Trigger.isInsert){
    List <Opportunity> OppListCreate = new List <Opportunity>();
       
    for(Account accnt : trigger.new){
        OppListCreate.add(New Opportunity(
        Name =   accnt.Name +   Date.Today().Month() + Date.Today().Year(),
        StageName = 'Prospecting',
        CloseDate = Date.Today() + 90,
        AccountId = accnt.Id ));
      
    }   
        if(OppListCreate.Size()>0){
            insert (OppListCreate);} 
        }
    // Check correct Opportunity name when update Account name
   if( Trigger.isUpdate){
       List <Opportunity> OppListUpd = new List <Opportunity>();
       Set <Id> OppIdUpd = new Set <Id>();
       for(Account a : trigger.old) {
         OppIdUpd.add(a.Id); } 
       List<Account> acclistUpd = [SELECT Id, Name, (SELECT Id, AccountId, Name, Amount 
                           FROM Opportunities)FROM Account WHERE Id IN :OppIdUpd]; 
        for(Account acc : acclistUpd) {
         for(Opportunity opp : acc.Opportunities) {
              // Check correct Opportunity name when update Account name
             if (!opp.Name.contains(acc.Name)){
                opp.Name =  acc.Name + Date.Today().Month() + Date.Today().Year();
                OppListUpd.add(opp);
             }} 
     }  
       update (OppListUpd);
   }

   // Automatic deleting all relative Opportunity when account deleted 
   if(Trigger.isDelete ){
   List <Opportunity> OppListDel = new List <Opportunity>();
      
   Set <Id> OppIdDel = new Set <Id>();
       for(Account a : trigger.old) {
         OppIdDel.add(a.Id); }
       
   Messaging.reserveSingleEmailCapacity(trigger.size);
   List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();      
   List<Account> acclist = [SELECT Id, Name, (SELECT Id, AccountId, Name, Amount 
                           FROM Opportunities)FROM Account WHERE Id IN :OppIdDel];                 
        for(Account acc : acclist) {
        String opplisttext = '<br/>';
        Integer countNumber = 1;
         for(Opportunity opp : acc.Opportunities) {
            OppListDel.add(opp);
            opplisttext += countNumber + '. '+ opp.Id + ', "' + opp.Name + '", ' + opp.Amount +  '<br/>' ;
            countNumber++;} 
            
         // Creating sending message for inform users about detail of deleting records account and relative opportunities
           Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
           email.setToAddresses(new String[] {UserInfo.getUserEmail()});
           email.setSubject('Deleted details');
           email.setHtmlBody('Hi, ' + new String[] {UserInfo.getName()} +','+ '<br/>'+'<br/>' + 'Due to deletion of Account "' +  acc.Name + 
                             '" all next related Opportunities were deleted as well:' + '<br/>' + opplisttext + '<br/>' + 'Best regards,'+
                             '<br/>' + 'Red Tag Service Team.');
           emails.add(email);
     }      
       
    Messaging.sendEmail(emails);
    delete (OppListDel);
   } 
}